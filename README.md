# Practica DE1 UACM 2021

Circuito de rectificación de media onda y onda completa

En la implementacion del circuito de rectificacion de media onda y onda completa, con el objetivo de poder hacer la conversion de corriente alterna a corriente directa. Para la implentacion de tales circuitos, se hizo uso de material electronico:
diodos 1N4004 
Puente de diodos de 2A 
Transformador de 1227 a 12V (de preferencia de 3A) 
Regulador LM317P 
Jack 3.5 (conector de audio) 
Alambre de cobre calibre 22 o cables tipo dupont 
Protoboard 
Capacitores electr´oliticos (varios valores) 
Capacitores cer´amicos (varios valores) 
Resistencias de varios valores
Resistencia de 10 Ω de 1 Watt 
Cables caim´an (por lomenos 3) 
Extensi´on (se romper´a para colocarla al tranformador) 
Switch (para el transformado) 
Cinta de aislar 

Rectificacion de media onda
Para la construccion de este circuito se tomo trasformador, cable dupot, fuente de corriente alterna,  tabla protoboard en el se monto el diodo (propocito general), acompañado con  una resistencia de carga conectados en serie. El cual nos permitio hacer una rectificacion de media onda. Con  ayuda del multimetro nos permitio medir la resistencia de carga del circuito rectificador. Con base a los calculos que se formularon, se coroboro el voltaje que fue medido con el multimetro.

Rectificador de onda completa 
Para  la construcccion de dicho circuito se hizo uso de una tabla protoboard, cable dupot, capacitores, puente de diodos, trasformador, regulador, resistencias. Se monto el puente de diodos, en el cual se conecto en paralelo a un capacitor1, puesteriormnte se conecto el regulador en serie, en el se conecto las resistencias de carga y de forma paralela un capacitor2.
La toma de mediciones del voltaje de rectificacion se uso el multimetro el cual nos arojo los voltajes correspondientes a los voltajes requeridos de rectificacion. Al igual para corroborar la señales que se obtuvieron se hizo uso del simulador circuitmaker.

